# full_network

A new Flutter network module project.

## Getting Started

This module will help you to call any api with achieve below point 

✅ No duplicate code
✅ Not blocking main thread and dropping FPS
✅ No error-proof codebase
✅ Minimised time on adding new services and API calls
✅ Structured and strongly-typed error handling


please check the below example to check if it's fitted with you and contact me if you want any support 

##1- add lib into pubspec.yaml


  full_network:
    git:
      url: git@gitlab.com:abedqa39/flutter_network_lib.git
      ref: main



##2- create model



 List<Todo> todoFromJson(List<dynamic> data) =>
    List<Todo>.from(data.map((x) => Todo.fromJson(x)));

String todoToJson(List<Todo> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Todo {
  Todo({
    this.userId,
    this.id,
    this.title,
    this.completed,
  });

  int? userId;
  int? id;
  String? title;
  bool? completed;

  factory Todo.fromJson(Map json) => Todo(
        userId: json["userId"],
        id: json["id"],
        title: json["title"],
        completed: json["completed"],
      );

  Map<String, dynamic> toJson() => {
        "userId": userId,
        "id": id,
        "title": title,
        "completed": completed,
      };
}


##3- call the api


await NetworkService(
      baseUrl: 'https://jsonplaceholder.typicode.com/',
    ).execute(
        const NetworkRequest(
          type: NetworkRequestType.GET,
          path: 'todos',
          data: NetworkRequestBody.json({}),
        ), (response) {
      return todoFromJson(response);
    }).then((value) {
      value.when(
        ok: (data) {
          print(data);
        },
        invalidParameters: (message) {
          print(message);
        },
        noAuth: (message) {
          print(message);
        },
        noAccess: (message) {
          print(message);
        },
        badRequest: (message) {
          print(message);
        },
        notFound: (message) {
          print(message);
        },
        conflict: (message) {
          print(message);
        },
        noData: (message) {
          print(message);
        },
      );
    });


full Reference from this link https://betterprogramming.pub/building-generic-and-performant-networking-layer-in-flutter-b25c2b1b89a4

